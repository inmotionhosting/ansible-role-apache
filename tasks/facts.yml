# tasks/facts.yml

- name: Include os_family vars
  include_vars: "{{ ansible_os_family }}.yml"
  vars:
    targets:
      - Debian
  when: ansible_os_family in targets

- name: (UltraStack) Include UltraStack vars
  include_vars: ultrastack.yml
  when:
    - use_ultrastack is defined
    - use_ultrastack

- name: Set Apache core facts
  set_fact:
    apache_daemon: "{{ apache_daemon | default('httpd') }}"
    apache_group: "{{ apache_group | default('apache') }}"
    apache_name: "{{ apache_name | default('httpd') }}"
    apache_user: "{{ apache_user | default('apache') }}"
    apache_port_http: "{{ apache_port_http | default(80) }}"
    apache_port_https: "{{ apache_port_https | default(443) }}"

- name: Check variable 'apache_daemon'
  fail:
    msg: |
      Invalid value for variable 'apache_daemon': '{{ apache_daemon }}'

      'apache_daemon' must be a non-null string.
  failed_when: >-
    apache_daemon is not string
    or apache_daemon == 0

- name: Check variable 'apache_group'
  fail:
    msg: |
      Invalid value for variable 'apache_group': '{{ apache_group }}'

      'apache_group' must be a non-null string.
  failed_when: >-
    apache_group is not string
    or apache_group == 0

- name: Check variable 'apache_name'
  fail:
    msg: |
      Invalid value for variable 'apache_name': '{{ apache_name }}'

      'apache_name' must be a non-null string.
  failed_when: >-
    apache_name is not string
    or apache_name == 0

- name: Check variable 'apache_user'
  fail:
    msg: |
      Invalid value for variable 'apache_user': '{{ apache_user }}'

      'apache_user' must be a non-null string.
  failed_when: >-
    apache_user is not string
    or apache_user == 0

- name: Check variable 'apache_port_http'
  fail:
    msg: |
      Invalid value for variable 'apache_port_http': '{{ apache_port_http }}'

      'apache_port_http' must be a number between 1-65535.
  failed_when: >-
    apache_port_http|int < 1
    or apache_port_http|int > 65535

- name: Check variable 'apache_port_https'
  fail:
    msg: |
      Invalid value for variable 'apache_port_https': '{{ apache_port_https }}'

      'apache_port_https' must be a number between 1-65535.
  failed_when: >-
    apache_port_https|int < 1
    or apache_port_https|int > 65535

- name: Set Apache config facts
  set_fact:
    apache_config: >-
      {{ apache_config |
      default("/etc/{{ apache_name }}/conf/{{ apache_name }}.conf") }}
    apache_config_path: >-
      {{ apache_config_path |
      default("/etc/{{ apache_name }}/conf.d") }}
    apache_config_ports: >-
      {{ apache_config_ports |
      default("{{ apache_config_path }}/ssl.conf") }}
    apache_config_site_path: >-
      {{ apache_config_site_path |
      default("{{ apache_config_path }}") }}
    apache_modules_path: >-
      {{ apache_modules_path |
      default("/etc/{{ apache_name }}/modules") }}
    apache_modules_config_path: >-
      {{ apache_modules_config_path |
      default("/etc/{{ apache_name }}/conf.modules.d") }}
    apache_packages: >-
      {{ apache_packages |
      default(["httpd", "httpd-tools", "mod_ssl"]) }}

- name: (Let's Encrypt) Add Apache to certbot_stop_services
  set_fact:
    certbot_stop_services: "{{ certbot_stop_services + [apache_name] }}"
  when:
    - certbot_stop_services is defined
    - apache_name not in certbot_stop_services

- name: (Let's Encrypt) Create certbot_stop_services with Apache
  set_fact:
    certbot_stop_services:
      - "{{ apache_name }}"
  when:
    - certbot_stop_services is not defined
